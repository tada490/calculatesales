package jp.alhinc.tada_shuto.calculate_sales;

class BranchFormatException extends Exception{
	BranchFormatException(String message){
		super(message);
	}
}

public class Branch {
	// constructor
	String code; // 支店コード
	String name; // 支店名
	Branch(String code, String name) throws BranchFormatException{
		this.code = code;
		this.name = name;
		if(this.code.length() != 3) {
			throw new BranchFormatException("支店定義ファイルのフォーマットが不正です");
		}
	}
}