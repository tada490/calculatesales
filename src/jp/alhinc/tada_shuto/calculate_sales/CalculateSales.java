package jp.alhinc.tada_shuto.calculate_sales;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class CalculateSales {
	public static void main(String[] args) {
		System.out.println("ここにあるファイルを開きます =>" + args[0]);
		BufferedReader br = null;
		// 支店定義ファイル読み込み処理
		try {
			File branchFile = new File(args[0], "branch.lst");
			FileReader bfr = new FileReader(branchFile);
			br = new BufferedReader(bfr);

			// Branchインスタンスの作成
			String line;
			ArrayList<Branch> branch = new ArrayList();
			while((line = br.readLine()) != null) {
;				String[] branchInfo = line.split(",", 2);
				branch.add(new Branch(branchInfo[0],branchInfo[1]));
			}
		} catch(FileNotFoundException e) {
			System.out.println("支店定義ファイルが存在しません");
		} catch(BranchFormatException e) {
			System.out.println(e);
		}

		// 集計処理
		try {
			File salesFile = new File(args[0]);
			File[] salesFiles = salesFile.listFiles();
			int i = 0;
			while(salesFiles != null) {
				FileReader fr = new FileReader(salesFiles[i]);
				br = new BufferedReader(fr);

				// Salesインスタンスの作成
				String line;
				ArrayList<Sales> sales = new ArrayList();
				while((line = br.readLine()) != null) {
					String[] salesInfo = line.split("¥¥n", 2);
					sales.add(new Sales(salesInfo[0],salesInfo[1]));
				}
				i++;
			}
		} catch() {

		}



		// 集計結果出力処理

		try {

		} catch() {

		} catch(Exception e) {
			System.out.println("予期せぬエラーが発生しました");
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("closeできませんでした。");
				}
			}
		}
	}

}
